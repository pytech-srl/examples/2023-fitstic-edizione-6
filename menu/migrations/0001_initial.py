# Generated by Django 4.1.7 on 2023-03-13 16:52

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='CategoriaDiCibo',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nome', models.CharField(max_length=255)),
                ('descrizione', models.TextField()),
            ],
        ),
        migrations.CreateModel(
            name='Pietanza',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nome', models.CharField(max_length=255)),
                ('descrizione', models.TextField()),
                ('categoria', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='menu.categoriadicibo')),
            ],
        ),
    ]
