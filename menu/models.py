from django.db import models



class CategoriaDiCibo(models.Model):

    nome = models.CharField(max_length=255)
    descrizione = models.TextField()


class Pietanza(models.Model):

    nome = models.CharField(max_length=255)
    descrizione = models.TextField()

    categoria = models.ForeignKey(
        CategoriaDiCibo, on_delete=models.PROTECT,
        related_name="pietanze"
    )
