from django.contrib import admin

from menu.models import CategoriaDiCibo, Pietanza

# Register your models here.
admin.site.register(CategoriaDiCibo)
admin.site.register(Pietanza)
