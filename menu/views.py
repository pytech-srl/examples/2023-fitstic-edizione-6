from django.contrib.auth.decorators import login_required
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.generic import (
    CreateView,
    DeleteView,
    DetailView,
    ListView,
    UpdateView,
)

from menu.models import CategoriaDiCibo

__all__ = [
    "CategoriaDiCiboCreateView",
    "CategoriaDiCiboDeleteView",
    "CategoriaDiCiboDetailView",
    "CategoriaDiCiboListView",
    "CategoriaDiCiboUpdateView",
]


class CategoriaDiCiboListView(ListView):
    model = CategoriaDiCibo


class CategoriaDiCiboDetailView(DetailView):
    model = CategoriaDiCibo


@method_decorator(login_required, name="dispatch")
class CategoriaDiCiboCreateView(CreateView):
    model = CategoriaDiCibo
    fields = ("nome", "descrizione")

    success_url = reverse_lazy("menu:lista_categorie")


@method_decorator(login_required, name="dispatch")
class CategoriaDiCiboUpdateView(UpdateView):
    model = CategoriaDiCibo
    fields = ("nome", "descrizione")

    def get_success_url(self):
        return reverse_lazy(
            "menu:dettaglio_categoria",
            kwargs={"pk": self.object.id}
        )


@method_decorator(login_required, name="dispatch")
class CategoriaDiCiboDeleteView(DeleteView):
    model = CategoriaDiCibo

    success_url = reverse_lazy("menu:lista_categorie")
